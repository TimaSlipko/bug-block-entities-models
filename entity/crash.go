package entity

import (
	"time"
	"gorm.io/datatypes"
	"gorm.io/gorm"
)

type Crash struct {
	gorm.Model
	ID            uint
	ApplicationID uint
	Email         string `gorm:"type:text"`
	Metadata      datatypes.JSON
	Log           string `gorm:"type:longtext"`
	Status        string `gorm:"type:text; default:'new'"`
	Size          int
	CreatedAt     time.Time
}
