package entity

import (
	"time"

	"gorm.io/gorm"
)

type Organization struct {
	gorm.Model
	ID          uint
	Name        string `gorm:"type:text"`
	Description string `gorm:"type:text"`
	Plan        string `gorm:"type:text; default:'free'"`
	PlanUntil   *time.Time
}
