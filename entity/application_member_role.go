package entity

import (
	"gorm.io/gorm"
)

type ApplicationMemberRole struct {
	gorm.Model
	ID             uint
	ApplicationID  uint
	OrganizationID *uint
	MemberID       uint
	Role           string `gorm:"type:text; default:'member'"`
	Status         string `gorm:"type:text; default:'pending'"`
}
