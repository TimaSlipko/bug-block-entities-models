package entity

import (
	"gorm.io/gorm"
)

type IssueIntegrationData struct {
	gorm.Model
	ID            uint
    IssueID       uint
    JiraIssueLink *string `gorm:"type:text`
}
