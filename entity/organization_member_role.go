package entity

import (
	"gorm.io/gorm"
)

type OrganizationMemberRole struct {
	gorm.Model
	ID             uint
	OrganizationID uint
	MemberID       uint
	Role           string `gorm:"type:text; default:'member'"`
}
