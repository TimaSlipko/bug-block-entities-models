package entity

import (
	"gorm.io/gorm"
)

type Member struct {
	gorm.Model
	ID         uint
	Name       string `gorm:"type:varchar(255)"`
	Password   string `gorm:"type:text"`
	Email      string `gorm:"type:text;unique"`
	AuthType   string `gorm:"type:text;default:'base'"`
	IsVerified int    `default:0"`
	Status     string `gorm:"type:text"`
	Role       string `default:'member'"`
}
