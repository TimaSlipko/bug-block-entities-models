package entity

import (
	"gorm.io/gorm"
)

type Integration struct {
	gorm.Model
	ID          uint
	Name        string `gorm:"type:text"`
	Description string `gorm:"type:text"`
	ImagePath   string `gorm:"type:text"`
	Type        string `gorm:"type:text"`
	Implemented int    `default:0"`
}
