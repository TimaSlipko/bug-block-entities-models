package entity

import (
	"gorm.io/gorm"
)

type JiraIntegrationConfiguration struct {
	gorm.Model
	ID               uint
	ApplicationID    uint
	Token            string `gorm:"type:text"`
	Email            string `gorm:"type:text"`
	ProjectKey       string `gorm:"type:text"`
	OrganizationName string `gorm:"type:text"`
	DefaultIssueType *string `gorm:"type:text"`
	Status           int    `default:0"`
}
