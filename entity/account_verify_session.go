package entity

import (
	"gorm.io/gorm"
)

type AccountVerifySession struct {
	gorm.Model
	ID       uint
	MemberID uint
	Key      string `gorm:"type:text"`
	IsUsed   int    `default:0"`
}
