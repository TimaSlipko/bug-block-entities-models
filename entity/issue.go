package entity

import (
	"time"
	"gorm.io/datatypes"
	"gorm.io/gorm"
)

type Issue struct {
	gorm.Model
	ID            uint
	ApplicationID uint
	Email         string `gorm:"type:text"`
	Type          string `gorm:"type:text"`
	Metadata      datatypes.JSON
	Logs          datatypes.JSON
	ImagePath     string `gorm:"type:text"`
	Description   string `gorm:"type:text"`
	Status        string `gorm:"type:text; default:'new'"`
	Size          int
	CreatedAt     time.Time
	JiraIssueLink *string `gorm:"type:text`
}
