package entity

import (
	"gorm.io/gorm"
)

type SlackIntegrationConfiguration struct {
	gorm.Model
	ID            uint
	ApplicationID uint
	WebhookURL    string `gorm:"type:text"`
	Status        int    `default:0"`
}
