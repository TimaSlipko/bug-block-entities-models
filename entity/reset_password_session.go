package entity

import (
	"gorm.io/gorm"
)

type ResetPasswordSession struct {
	gorm.Model
	ID     uint
	UserID uint
	Token  string `gorm:"type:text"`
	IsUsed int    `default:0"`
}
