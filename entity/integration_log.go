package entity

import (
	"gorm.io/datatypes"
	"gorm.io/gorm"
)

type IntegrationLog struct {
	gorm.Model
	ID              uint
	IntegrationType string `gorm:"type:text"`
	Log             datatypes.JSON
	ResponseError   *string `gorm:"type:text"`
	ApplicationID   *uint
}
