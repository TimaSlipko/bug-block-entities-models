package entity

import (
	"gorm.io/datatypes"
	"gorm.io/gorm"
)

type Application struct {
	gorm.Model
	ID             uint
	OrganizationID *uint
	Name           string `gorm:"type:text"`
	Description    string `gorm:"type:text"`
	Type           string `gorm:"type:text; default:'ios'"`
	Status         string `gorm:"type:text; default:'pending'"`
	Key            string `gorm:"type:text"`
	Metadata       datatypes.JSON
}
