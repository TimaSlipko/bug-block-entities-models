package entity

import (
	"gorm.io/gorm"
)

type MemberEmailIntegrationConfiguration struct {
	gorm.Model
	ID                  uint
	ApplicationMemberID uint
	Status              int `default:0"`
}
