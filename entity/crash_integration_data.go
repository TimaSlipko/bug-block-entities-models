package entity

import (
	"gorm.io/gorm"
)

type CrashIntegrationData struct {
	gorm.Model
	ID            uint
    CrashID       uint
    JiraIssueLink *string `gorm:"type:text`
}
