package models

import (
	"gitlab.com/TimaSlipko/bug-block-entities-models/entity"
)

type AccountVerifySession struct {
	ID       uint   `json:"id"`
	MemberID uint   `json:"member_id"`
	Key      string `json:"key"`
	IsUsed   int    `json:"is_used"`
}

func GetAccountVerifySession(accountVerifySession entity.AccountVerifySession) AccountVerifySession {
	accountVerifySessionModel := AccountVerifySession{}
	accountVerifySessionModel.ID = accountVerifySession.ID
	accountVerifySessionModel.Key = accountVerifySession.Key
	accountVerifySessionModel.MemberID = accountVerifySession.MemberID
	accountVerifySessionModel.IsUsed = accountVerifySession.IsUsed
	return accountVerifySessionModel
}

func GetAccountVerifySessionArray(array []entity.AccountVerifySession) []AccountVerifySession {
	formatted := []AccountVerifySession{}
	for _, arrayEl := range array {
		formatted = append(formatted, GetAccountVerifySession(arrayEl))
	}

	return formatted
}
