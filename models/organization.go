package models

import (
	"time"

	"gitlab.com/TimaSlipko/bug-block-entities-models/entity"
)

type Organization struct {
	ID          uint       `json:"id"`
	Name        string     `json:"name"`
	Description string     `json:"description"`
	Plan        string     `json:"plan"`
	PlanUntil   *time.Time `json:"plan_until"`
}

func GetOrganization(organization entity.Organization) Organization {
	organizationModel := Organization{}
	organizationModel.ID = organization.ID
	organizationModel.Name = organization.Name
	organizationModel.Description = organization.Description
	organizationModel.Plan = organization.Plan
	organizationModel.PlanUntil = organization.PlanUntil
	return organizationModel
}

func GetOrganizationArray(array []entity.Organization) []Organization {
	formatted := []Organization{}
	for _, arrayEl := range array {
		formatted = append(formatted, GetOrganization(arrayEl))
	}

	return formatted
}
