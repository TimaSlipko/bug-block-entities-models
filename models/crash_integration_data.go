package models

import (
	"gitlab.com/TimaSlipko/bug-block-entities-models/entity"
)

type CrashIntegrationData struct {
    ID            uint `json:"id"`
    CrashID       uint `json:"crash_id"`
    JiraIssueLink *string `json:"jira_issue_link"`
}

func GetCrashIntegrationData(crashIntegrationData entity.CrashIntegrationData) CrashIntegrationData {
	crashIntegrationDataModel := CrashIntegrationData{}
	crashIntegrationDataModel.ID = crashIntegrationData.ID
    crashIntegrationDataModel.CrashID = crashIntegrationData.CrashID
    crashIntegrationDataModel.JiraIssueLink = crashIntegrationData.JiraIssueLink
	return crashIntegrationDataModel
}

func GetCrashIntegrationDataArray(array []entity.CrashIntegrationData) []CrashIntegrationData {
	formatted := []CrashIntegrationData{}
	for _, arrayEl := range array {
		formatted = append(formatted, GetCrashIntegrationData(arrayEl))
	}

	return formatted
}
