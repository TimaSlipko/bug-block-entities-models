package models

import (
	"time"
	"gitlab.com/TimaSlipko/bug-block-entities-models/entity"

	"gorm.io/datatypes"
)

type Issue struct {
	ID            uint           `json:"id"`
	ApplicationID uint           `json:"application_id"`
	Email         string         `json:"email"`
	Type          string         `json:"type"`
	Metadata      datatypes.JSON `json:"metadata"`
	Logs          datatypes.JSON `json:"logs"`
	ImagePath     string         `json:"image_path"`
	Description   string         `json:"description"`
	Status        string         `json:"status"`
	Size          int            `json:"size"`
	Timestamp     time.Time      `json:"timestamp"`
	JiraIssueLink *string        `json:"jira_issue_link"`
}

func GetIssue(issue entity.Issue) Issue {
	issueModel := Issue{}
	issueModel.ID = issue.ID
	issueModel.ApplicationID = issue.ApplicationID
	issueModel.Email = issue.Email
	issueModel.Type = issue.Type
	issueModel.Metadata = issue.Metadata
	issueModel.Logs = issue.Logs
	issueModel.ImagePath = issue.ImagePath
	issueModel.Description = issue.Description
	issueModel.Status = issue.Status
	issueModel.Size = issue.Size
	issueModel.Timestamp = issue.CreatedAt
	issueModel.JiraIssueLink = issue.JiraIssueLink
	return issueModel
}

func GetIssueArray(array []entity.Issue) []Issue {
	formatted := []Issue{}
	for _, arrayEl := range array {
		formatted = append(formatted, GetIssue(arrayEl))
	}

	return formatted
}
