package models

import (
	"gitlab.com/TimaSlipko/bug-block-entities-models/entity"
)

type MemberEmailIntegrationConfiguration struct {
	ID                  uint `json:"id"`
	ApplicationMemberID uint `json:"application_member_id"`
	Status              int  `json:"status"`
}

func GetMemberEmailIntegrationConfiguration(memberEmailIntegrationConfiguration entity.MemberEmailIntegrationConfiguration) MemberEmailIntegrationConfiguration {
	memberEmailIntegrationConfigurationModel := MemberEmailIntegrationConfiguration{}
	memberEmailIntegrationConfigurationModel.ID = memberEmailIntegrationConfiguration.ID
	memberEmailIntegrationConfigurationModel.ApplicationMemberID = memberEmailIntegrationConfiguration.ApplicationMemberID
	memberEmailIntegrationConfigurationModel.Status = memberEmailIntegrationConfiguration.Status
	return memberEmailIntegrationConfigurationModel
}

func GetMemberEmailIntegrationConfigurationArray(array []entity.MemberEmailIntegrationConfiguration) []MemberEmailIntegrationConfiguration {
	formatted := []MemberEmailIntegrationConfiguration{}
	for _, arrayEl := range array {
		formatted = append(formatted, GetMemberEmailIntegrationConfiguration(arrayEl))
	}

	return formatted
}
