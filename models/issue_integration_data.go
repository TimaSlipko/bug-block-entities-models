package models

import (
	"gitlab.com/TimaSlipko/bug-block-entities-models/entity"
)

type IssueIntegrationData struct {
    ID            uint `json:"id"`
    IssueID       uint `json:"issue_id"`
    JiraIssueLink *string `json:"jira_issue_link"`
}

func GetIssueIntegrationData(issueIntegrationData entity.IssueIntegrationData) IssueIntegrationData {
	issueIntegrationDataModel := IssueIntegrationData{}
	issueIntegrationDataModel.ID = issueIntegrationData.ID
    issueIntegrationDataModel.IssueID = issueIntegrationData.IssueID
    issueIntegrationDataModel.JiraIssueLink = issueIntegrationData.JiraIssueLink
	return issueIntegrationDataModel
}

func GetIssueIntegrationDataArray(array []entity.IssueIntegrationData) []IssueIntegrationData {
	formatted := []IssueIntegrationData{}
	for _, arrayEl := range array {
		formatted = append(formatted, GetIssueIntegrationData(arrayEl))
	}

	return formatted
}
