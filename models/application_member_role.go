package models

import (
	"gitlab.com/TimaSlipko/bug-block-entities-models/entity"
)

type ApplicationMemberRole struct {
	ID             uint   `json:"id"`
	ApplicationID  uint   `json:"application_id"`
	OrganizationID *uint  `json:"organization_id"`
	MemberID       uint   `json:"member_email"`
	Role           string `json:"role"`
	Status         string `json:"status"`
}

func GetApplicationMemberRole(appMemberRole entity.ApplicationMemberRole) ApplicationMemberRole {
	appMemberRoleModel := ApplicationMemberRole{}
	appMemberRoleModel.ID = appMemberRole.ID
	appMemberRoleModel.ApplicationID = appMemberRole.ApplicationID
	appMemberRoleModel.OrganizationID = appMemberRole.OrganizationID
	appMemberRoleModel.MemberID = appMemberRole.MemberID
	appMemberRoleModel.Role = appMemberRole.Role
	appMemberRoleModel.Status = appMemberRole.Status
	return appMemberRoleModel
}

func GetApplicationMemberRoleArray(array []entity.ApplicationMemberRole) []ApplicationMemberRole {
	formatted := []ApplicationMemberRole{}
	for _, arrayEl := range array {
		formatted = append(formatted, GetApplicationMemberRole(arrayEl))
	}

	return formatted
}
