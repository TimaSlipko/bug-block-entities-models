package models

import (
	"time"
	"gitlab.com/TimaSlipko/bug-block-entities-models/entity"

	"gorm.io/datatypes"
)

type Crash struct {
	ID            uint           `json:"id"`
	ApplicationID uint           `json:"application_id"`
	Email         string         `json:"email"`
	Metadata      datatypes.JSON `json:"metadata"`
	Log           string         `json:"log"`
	Status        string         `json:"status"`
	Size          int            `json:"size"`
	Timestamp     time.Time      `json:"timestamp"`
}

func GetCrash(crash entity.Crash) Crash {
	crashModel := Crash{}
	crashModel.ID = crash.ID
	crashModel.ApplicationID = crash.ApplicationID
	crashModel.Email = crash.Email
	crashModel.Metadata = crash.Metadata
	crashModel.Log = crash.Log
	crashModel.Status = crash.Status
	crashModel.Size = crash.Size
	crashModel.Timestamp = crash.CreatedAt
	return crashModel
}

func GetCrashArray(array []entity.Crash) []Crash {
	formatted := []Crash{}
	for _, arrayEl := range array {
		formatted = append(formatted, GetCrash(arrayEl))
	}

	return formatted
}
