package models

import (
	"gitlab.com/TimaSlipko/bug-block-entities-models/entity"
)

type Integration struct {
	ID          uint   `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	ImagePath   string `json:"image_path"`
	Type        string `json:"type"`
	State       string `json:"state"`
	Implemented int    `json:"implemented"`
}

func GetIntegration(integration entity.Integration) Integration {
	integrationModel := Integration{}
	integrationModel.ID = integration.ID
	integrationModel.Name = integration.Name
	integrationModel.Description = integration.Description
	integrationModel.ImagePath = integration.ImagePath
	integrationModel.Type = integration.Type
	integrationModel.Implemented = integration.Implemented
	return integrationModel
}

func GetIntegrationArray(array []entity.Integration) []Integration {
	formatted := []Integration{}
	for _, arrayEl := range array {
		formatted = append(formatted, GetIntegration(arrayEl))
	}

	return formatted
}
