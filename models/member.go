package models

import (
	"gitlab.com/TimaSlipko/bug-block-entities-models/entity"
)

type Member struct {
	ID         uint   `json:"id"`
	Email      string `json:"email"`
	Name       string `json:"name"`
	AuthType   string `json:"auth_type"`
	IsVerified int    `json:"is_verified"`
	Status     string `json:"status"`
	Role       string `json:"role"`
}

func GetMember(member entity.Member) Member {
	memberModel := Member{}
	memberModel.ID = member.ID
	memberModel.Name = member.Name
	memberModel.Email = member.Email
	memberModel.AuthType = member.AuthType
	memberModel.IsVerified = member.IsVerified
	memberModel.Status = member.Status
	memberModel.Role = member.Role

	return memberModel
}

func GetMemberArray(array []entity.Member) []Member {
	formatted := []Member{}
	for _, arrayEl := range array {
		formatted = append(formatted, GetMember(arrayEl))
	}

	return formatted
}
