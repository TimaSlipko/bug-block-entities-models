package models

import "gorm.io/datatypes"

type EmailIntegrationJSONModel struct {
	Cause               string         `json:"cause"`
	TemplateID          string         `json:"template_id"`
	FromEmailAddress    string         `json:"from_email_address"`
	ToEmailAddress      string         `json:"to_email_address"`
	ToUserID            uint           `json:"to_user_id"`
	DynamicData         datatypes.JSON `json:"dynamic_data"`
	DeliveredStatus     string         `json:"delivered_status"`
	MessageID           string         `json:"message_id"`
}
