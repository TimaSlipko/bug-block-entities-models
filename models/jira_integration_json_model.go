package models

import "gorm.io/datatypes"

type JiraIntegrationJSONModel struct {
	Cause          string                       `json:"cause"`
	Configuration  JiraIntegrationConfiguration `json:"configuration"`
	DynamicData    datatypes.JSON               `json:"dynamic_data"`
}
