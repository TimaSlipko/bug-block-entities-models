package models

import (
	"gitlab.com/TimaSlipko/bug-block-entities-models/entity"

	"gorm.io/datatypes"
)

type IntegrationLog struct {
	ID              uint           `json:"id"`
	IntegrationType string         `json:"integration_type"`
	Log             datatypes.JSON `json:"log"`
	ResponseError   *string        `json:"response_error"`
	ApplicationID   *uint          `json:"application_id"`
}

func GetIntegrationLog(integrationLog entity.IntegrationLog) IntegrationLog {
	integrationLogModel := IntegrationLog{}
	integrationLogModel.ID = integrationLog.ID
	integrationLogModel.IntegrationType = integrationLog.IntegrationType
	integrationLogModel.Log = integrationLog.Log
	integrationLogModel.ResponseError = integrationLog.ResponseError
	integrationLogModel.ApplicationID = integrationLog.ApplicationID

	return integrationLogModel
}

func GetIntegrationLogArray(array []entity.IntegrationLog) []IntegrationLog {
	formatted := []IntegrationLog{}
	for _, arrayEl := range array {
		formatted = append(formatted, GetIntegrationLog(arrayEl))
	}

	return formatted
}
