package models

import "gorm.io/datatypes"

type SlackIntegrationJSONModel struct {
	Cause          string                        `json:"cause"`
	Configuration  SlackIntegrationConfiguration `json:"configuration"`
	DynamicData    datatypes.JSON                `json:"dynamic_data"`
}
