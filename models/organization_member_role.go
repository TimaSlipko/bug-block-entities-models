package models

import (
	"gitlab.com/TimaSlipko/bug-block-entities-models/entity"
)

type OrganizationMemberRole struct {
	ID             uint   `json:"id"`
	OrganizationID uint   `json:"organization_id"`
	MemberID       uint   `json:"member_email"`
	Role           string `json:"role"`
}

func GetOrganizationMemberRole(organizationMemberRole entity.OrganizationMemberRole) OrganizationMemberRole {
	organizationMemberRoleModel := OrganizationMemberRole{}
	organizationMemberRoleModel.ID = organizationMemberRole.ID
	organizationMemberRoleModel.OrganizationID = organizationMemberRole.OrganizationID
	organizationMemberRoleModel.MemberID = organizationMemberRole.MemberID
	organizationMemberRoleModel.Role = organizationMemberRole.Role
	return organizationMemberRoleModel
}

func GetOrganizationMemberRoleArray(array []entity.OrganizationMemberRole) []OrganizationMemberRole {
	formatted := []OrganizationMemberRole{}
	for _, arrayEl := range array {
		formatted = append(formatted, GetOrganizationMemberRole(arrayEl))
	}

	return formatted
}
