package models

import (
	"gitlab.com/TimaSlipko/bug-block-entities-models/entity"
)

type JiraIntegrationConfiguration struct {
	ID               uint   `json:"id"`
	ApplicationID    uint   `json:"application_id"`
	Token            string `json:"token"`
	Email            string `json:"email"`
	ProjectKey       string `json:"project_key"`
	OrganizationName string `json:"organization_name"`
	DefaultIssueType *string `json:"default_issue_type"`
	Status           int    `json:"status"`
}

func GetJiraIntegrationConfiguration(jiraIntegrationConfiguration entity.JiraIntegrationConfiguration) JiraIntegrationConfiguration {
	jiraIntegrationConfigurationModel := JiraIntegrationConfiguration{}
	jiraIntegrationConfigurationModel.ID = jiraIntegrationConfiguration.ID
	jiraIntegrationConfigurationModel.ApplicationID = jiraIntegrationConfiguration.ApplicationID
	jiraIntegrationConfigurationModel.Token = jiraIntegrationConfiguration.Token
	jiraIntegrationConfigurationModel.Email = jiraIntegrationConfiguration.Email
	jiraIntegrationConfigurationModel.ProjectKey = jiraIntegrationConfiguration.ProjectKey
	jiraIntegrationConfigurationModel.OrganizationName = jiraIntegrationConfiguration.OrganizationName
	jiraIntegrationConfigurationModel.DefaultIssueType = jiraIntegrationConfiguration.DefaultIssueType
	jiraIntegrationConfigurationModel.Status = jiraIntegrationConfiguration.Status
	return jiraIntegrationConfigurationModel
}

func GetJiraIntegrationConfigurationArray(array []entity.JiraIntegrationConfiguration) []JiraIntegrationConfiguration {
	formatted := []JiraIntegrationConfiguration{}
	for _, arrayEl := range array {
		formatted = append(formatted, GetJiraIntegrationConfiguration(arrayEl))
	}

	return formatted
}
