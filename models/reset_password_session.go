package models

import (
	"gitlab.com/TimaSlipko/bug-block-entities-models/entity"
)

type ResetPasswordSession struct {
	ID     uint   `json:"id"`
	UserID uint   `json:"user_id"`
	Token  string `json:"token"`
	IsUsed int    `json:"is_used"`
}

func GetResetPasswordSession(resetPasswordSession entity.ResetPasswordSession) ResetPasswordSession {
	resetPasswordSessionModel := ResetPasswordSession{}
	resetPasswordSessionModel.ID = resetPasswordSession.ID
	resetPasswordSessionModel.Token = resetPasswordSession.Token
	resetPasswordSessionModel.UserID = resetPasswordSession.UserID
	resetPasswordSessionModel.IsUsed = resetPasswordSession.IsUsed
	return resetPasswordSessionModel
}

func GetResetPasswordSessionArray(array []entity.ResetPasswordSession) []ResetPasswordSession {
	formatted := []ResetPasswordSession{}
	for _, arrayEl := range array {
		formatted = append(formatted, GetResetPasswordSession(arrayEl))
	}

	return formatted
}
