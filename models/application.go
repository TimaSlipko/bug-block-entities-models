package models

import (
	"gitlab.com/TimaSlipko/bug-block-entities-models/entity"

	"gorm.io/datatypes"
)

type Application struct {
	ID             uint           `json:"id"`
	OrganizationID *uint          `json:"organization_id"`
	Name           string         `json:"name"`
	Description    string         `json:"description"`
	Type           string         `json:"type"`
	Status         string         `json:"status"`
	Key            string         `json:"key"`
	Metadata       datatypes.JSON `json:"metadata"`
}

func GetApplication(app entity.Application) Application {
	appModel := Application{}
	appModel.ID = app.ID
	appModel.OrganizationID = app.OrganizationID
	appModel.Name = app.Name
	appModel.Description = app.Description
	appModel.Type = app.Type
	appModel.Status = app.Status
	appModel.Key = app.Key
	appModel.Metadata = app.Metadata
	return appModel
}

func GetApplicationArray(array []entity.Application) []Application {
	formatted := []Application{}
	for _, arrayEl := range array {
		formatted = append(formatted, GetApplication(arrayEl))
	}

	return formatted
}
